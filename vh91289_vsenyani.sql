-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- Хост: 83.69.230.13
-- Время создания: Июн 01 2015 г., 18:06
-- Версия сервера: 5.1.67-log
-- Версия PHP: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `vh91289_vsenyani`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `code` text NOT NULL,
  `descr` text NOT NULL,
  `geo_lon` double NOT NULL,
  `geo_lat` double NOT NULL,
  `disrtict_count` int(11) NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `region_id`, `name`, `code`, `descr`, `geo_lon`, `geo_lat`, `disrtict_count`, `enabled`) VALUES
(1, 2, 'Новокузнецк', 'novokuznetsk', 'Новокузнецк', 87.1366882324171, 53.7577154494656, 2, 1),
(2, 2, 'Кемерово', 'kemerovo', 'Кемерово', 86.0864639282187, 55.3538400593729, 0, 1),
(3, 1, 'Новосибирск', 'novosibirsk', 'Новосибирск', 82.9206848144469, 55.0295691277643, 0, 1),
(4, 1, 'Бердск', 'berdsk', 'Бердск', 83.1074523925725, 54.7582738100616, 0, 1),
(5, 3, 'Томск', 'tomsk', '', 84.9513497016227, 56.4838964983337, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(250) NOT NULL,
  `descr` text NOT NULL,
  `geo_lon` double NOT NULL,
  `geo_lat` double NOT NULL,
  `region_count` int(11) NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `countries`
--

INSERT INTO `countries` (`id`, `name`, `code`, `descr`, `geo_lon`, `geo_lat`, `region_count`, `enabled`) VALUES
(1, 'Россия', 'russia', 'Российская федерация', 92.4609375, 62.8105495674439, 2, 1),
(2, 'Беларусь', 'belarus', '', 28.3188145876011, 53.4507733124383, 0, 1),
(3, 'Украина', 'ukraine', '', 30.673828125, 49.0118401998483, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `descr` text NOT NULL,
  `geo_lon` double NOT NULL,
  `geo_lat` double NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `districts`
--

INSERT INTO `districts` (`id`, `city_id`, `name`, `descr`, `geo_lon`, `geo_lat`, `enabled`) VALUES
(1, 1, 'Центральный', 'Центральный район г. Новокузнецка', 87.1195220947229, 53.7682935575675, 1),
(2, 1, 'Заводской', 'Заводской район г. Новокузнецка', 87.2348785400332, 53.8256128299104, 1),
(3, 1, 'Куйбышевский', 'Куйбышевский район г. Новокузнецка', 87.0810699462863, 53.7247434109925, 1),
(4, 2, 'Рудничный', 'Рудничный район г. Кемерово', 86.1149597167952, 55.3937235819482, 1),
(5, 2, 'Ленинский', 'Ленинский район г. Кемерово', 86.1795043945296, 55.362054674306, 1),
(6, 3, 'Центральный', 'Центральный район г. Новосибирска', 82.928237915035, 55.0396256890268, 1),
(7, 5, 'Ленинский район', 'Ленинский район г. Томск', 84.9613587140716, 56.5275638446336, 1),
(8, 5, 'Октябрьский', 'Октябрьский район г. Томск', 85.0534057617187, 56.5230097385152, 1),
(9, 5, 'Советский', 'Советский район г. Томск', 85.0682008004182, 56.4735296313854, 1),
(10, 5, 'Кировский', 'Кировский район г. Томск', 84.9395547269978, 56.4589642516164, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `empfields`
--

CREATE TABLE IF NOT EXISTS `empfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Поля профиля работника' AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `empfields`
--

INSERT INTO `empfields` (`id`, `spec_id`, `name`, `type`, `enabled`) VALUES
(1, 1, 'няня - поле 1', 'text', 1),
(2, 1, 'Няня - поле 2', 'text', 1),
(3, 2, 'Навыки и умения', 'text', 1),
(4, 3, 'Опыт работы с детьми', 'text', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `empfieldvalues`
--

CREATE TABLE IF NOT EXISTS `empfieldvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empfield_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `enabled` smallint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `empfieldvalues`
--

INSERT INTO `empfieldvalues` (`id`, `empfield_id`, `emp_id`, `value`, `enabled`) VALUES
(1, 1, 1, 'тра-ля-ля', 1),
(2, 3, 3, 'Пылесос, стир. машина, полотер', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `emps`
--

CREATE TABLE IF NOT EXISTS `emps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `spec_id` int(11) NOT NULL,
  `nation_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `street` varchar(250) NOT NULL,
  `building` varchar(250) NOT NULL,
  `apartment` varchar(250) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `geo_lon` varchar(50) NOT NULL,
  `geo_lat` varchar(50) NOT NULL,
  `experience` int(11) NOT NULL,
  `photo` text NOT NULL,
  `descr` text NOT NULL,
  `rate` double NOT NULL,
  `opts` varchar(250) NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `emps`
--

INSERT INTO `emps` (`id`, `user_id`, `name`, `spec_id`, `nation_id`, `country_id`, `region_id`, `city_id`, `district_id`, `street`, `building`, `apartment`, `phone`, `geo_lon`, `geo_lat`, `experience`, `photo`, `descr`, `rate`, `opts`, `enabled`) VALUES
(1, 7, 'Няня 1', 1, 1, 1, 2, 1, 1, 'Кирова', '78', '12', '2147483647', '87.091171', '53.779885', 0, '', 'blah blah blah', 100, '0', 1),
(2, 10, 'Няня 2', 1, 1, 1, 2, 1, 2, 'Климасенко', '23', '14', '2147483647', '87.200348', '53.793259', 5, '', '0', 120, '0', 1),
(3, 7, 'Домработница 1', 2, 1, 1, 2, 1, 1, 'Строителей', '17', '21', '2147483647', '86.916076', '53.717164', 0, '', '0', 0, '0', 1),
(4, 10, 'Домработница 2', 2, 1, 1, 2, 2, 1, 'Олега Кошевого', '2', '1', '0', '86.049529', '55.530904', 0, '', '0', 0, '0', 1),
(5, 7, 'Сиделка 1', 3, 1, 1, 2, 2, 4, 'Связная', '23', '', '0', '86.1069985978174', '55.40082588063554', 0, '', '0', 0, '0', 1),
(6, 10, 'Сиделка 2', 3, 1, 1, 1, 3, 8, 'Выборная', '125/3', '', '0', '83.0201899277801', '55.003672010967215', 0, '', '0', 0, '0', 1),
(7, 7, 'Попугай', 1, 5, 1, 2, 1, 1, 'Грдины', '22', '', '123', '87.17294853439331', '53.75409698674561', 0, '', '0', 0, '0', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `role` int(11) NOT NULL,
  `descr` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `role`, `descr`) VALUES
(1, 'Администраторы', 1, 'Полные права'),
(2, 'Пользователи', 2, 'Право редактирования собственного аккаунта, просмотра профилей работников, списков регионов, городов и районов, специальностей, национальностей.');

-- --------------------------------------------------------

--
-- Структура таблицы `groups_permissions`
--

CREATE TABLE IF NOT EXISTS `groups_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `groups_permissions`
--

INSERT INTO `groups_permissions` (`id`, `group_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(7, 2, 4),
(8, 2, 5),
(9, 2, 6),
(10, 2, 7),
(11, 2, 8),
(12, 2, 9),
(13, 2, 10),
(14, 2, 11);

-- --------------------------------------------------------

--
-- Структура таблицы `groups_users`
--

CREATE TABLE IF NOT EXISTS `groups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `groups_users`
--

INSERT INTO `groups_users` (`id`, `group_id`, `user_id`) VALUES
(4, 1, 7),
(6, 2, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `nations`
--

CREATE TABLE IF NOT EXISTS `nations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `nations`
--

INSERT INTO `nations` (`id`, `name`, `enabled`) VALUES
(1, 'Русская', 1),
(2, 'Белоруска', 1),
(3, 'Украинка', 1),
(4, 'Молдаванка', 1),
(5, 'Полька', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `name`) VALUES
(1, '*'),
(2, 'search:*'),
(3, 'users:*'),
(4, 'regions:view'),
(5, 'cities:view'),
(6, 'search:view'),
(7, 'districts:view'),
(8, 'specs:view'),
(9, 'nations:view'),
(10, 'counrties:view'),
(11, 'users:login');

-- --------------------------------------------------------

--
-- Структура таблицы `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `code` text NOT NULL,
  `descr` text NOT NULL,
  `geo_lon` double NOT NULL,
  `geo_lat` double NOT NULL,
  `city_count` int(11) NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `regions`
--

INSERT INTO `regions` (`id`, `country_id`, `name`, `code`, `descr`, `geo_lon`, `geo_lat`, `city_count`, `enabled`) VALUES
(1, 1, 'Новосибирская область', 'novosib', 'Новосибирская область', 79.6289062499976, 55.2586606620166, 2, 1),
(2, 1, 'Кемеровская область', 'kemerovo', 'Кемеровская область', 87.1435546874968, 54.7026411359331, 2, 1),
(3, 1, 'Томская область', 'tomsk', '', 84.979248046875, 56.536291011749, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `descr` text NOT NULL,
  `price` double NOT NULL,
  `period` int(11) NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `name`, `descr`, `price`, `period`, `enabled`) VALUES
(1, 'Создание дополнительного профиля работника', 'Создание дополнительного профиля работника, если число уже созданных профилей превышает 3, допускается после оплаты. Услуга действует бессрочно.', 100, -1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `services_users`
--

CREATE TABLE IF NOT EXISTS `services_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `services_users`
--

INSERT INTO `services_users` (`id`, `service_id`, `user_id`) VALUES
(1, 7, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `value` text NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `enabled`) VALUES
(1, 'emps_count', '3', 1),
(2, 'seo_title', 'Все няни здесь', 1),
(3, 'seo_descr', 'Сервис поиска нянь, домработниц и сиделок', 1),
(4, 'seo_keywords', 'Няня, домработница, сиделка, найти, поиск', 1),
(5, 'vacancies_count', '1', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `spec`
--

CREATE TABLE IF NOT EXISTS `spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `spec`
--

INSERT INTO `spec` (`id`, `name`, `enabled`) VALUES
(1, 'Няня', 1),
(2, 'Домработница', 1),
(3, 'Сиделка', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email_address` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `emps_count` int(5) NOT NULL,
  `vacancies_count` int(5) NOT NULL,
  `balance` double NOT NULL DEFAULT '0',
  `enabled` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `email_address`, `created`, `modified`, `last_activity`, `emps_count`, `vacancies_count`, `balance`, `enabled`) VALUES
(7, 'admin', 'adb25332eb8bfcad71a6998b7d747140d0d163e6', 'Админ', 'webdepo@list.ru', '2014-05-03 14:05:07', '2014-05-03 14:05:07', '0000-00-00 00:00:00', 0, 0, 1, 1),
(10, 'user', 'abf3e1c0cc8df899c8ba19403ea36d7b11ee54c9', 'Пользователь', 'helenlazar@yandex.ru', '2014-05-25 09:40:36', '2014-05-30 11:00:34', '2033-12-31 17:00:00', 0, 0, 25, 1),
(11, 'SoWah', 'bd5a870bb73216088208b218b01881b2a5f966fd', '', 'super.avt@mail.ru', '2014-10-07 10:17:44', '2014-10-07 10:17:44', '0000-00-00 00:00:00', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `vacancies`
--

CREATE TABLE IF NOT EXISTS `vacancies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `spec_id` int(11) NOT NULL,
  `nation_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `rate` double NOT NULL,
  `name` varchar(250) NOT NULL,
  `descr` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `vacancies`
--

INSERT INTO `vacancies` (`id`, `user_id`, `spec_id`, `nation_id`, `region_id`, `city_id`, `district_id`, `rate`, `name`, `descr`, `created`, `modified`, `enabled`) VALUES
(1, 7, 1, 0, 1, 3, 6, 120, 'Ищу няню для ребенка 1.5 лет в Центральном районе г. Новосибирска', 'без в/п, для ребенка 1.5 лет, на полный день', '2014-05-17 21:11:02', '2014-05-17 21:11:02', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
