<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	<?php
		echo $this->Html->meta('description',!empty($settings["seo_descr"])?$settings["seo_descr"]:"");
		echo $this->Html->meta('keywords',!empty($settings["seo_keywords"])?$settings["seo_keywords"]:"");	
		
		echo $this->Html->meta('icon');

		echo $this->Html->css('style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		if($this->request->controller =='search'){
		echo $this->Html->script('http://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru_RU');
		}
		echo $this->Html->script(array('jquery-1.11.0', 'scripts'));
		echo $this->Html->scriptBlock('
		var MyjQ=new jQuery.noConflict();
		');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
		<div id="header">
			<h1><?php echo $this->Html->link('Все няни здесь', 'http://'.$_SERVER['HTTP_HOST']); ?></h1>
			<ul id="UserMenu" class="usermenu">
				<?php
				if((empty($Auth))||(!isset($Auth['id']))){
				?>
				<li><?php echo $this->Html->link('Вход', '/users/login/'); ?></li>
				<li><?php echo $this->Html->link('Регистрация', '/users/register/'); ?></li>
				<?php
				}
				else{
				?>
				<li><?php echo $this->Html->link('Личный кабинет '.$Auth['username'], '/users/view/'.$Auth['id']); ?></li>
				<li><?php echo $this->Html->link('Выход', '/users/logout/'); ?></li>
				<?php
				}
				?>
			</ul>
			<form id="FilterForm"class="filter_form" method="POST" action="">
				<div class="filter_elm">
					<label for="city_id">Страна:</label>
					<select name="country_id" onchange="this.form.submit();">
						<option value="0"></option>
						<?php
						foreach($countries as $k=>$v){
						?>
						<option value="<?php echo $k; ?>"<?php echo (isset($pst['country_id'])?($pst['country_id']==$k?' selected':''):(isset($default_country_id)&&($default_country_id==$k)?' selected':'')); ?>><?php echo $v; ?></option>
						
						<?php
						}
						?>
					</select>
				</div>

				<div class="filter_elm">
					<label for="region_id">Регион:</label>
					<select name="region_id" onchange="this.form.submit();">
						<option value="0"></option>
						<?php
						foreach($regions as $k=>$v){
						?>
						<option value="<?php echo $k; ?>"<?php echo ((isset($pst['region_id'])&&$pst['region_id']==$k)?' selected':''); ?>><?php echo $v; ?></option>
						<?php
						}
						?>
					</select>
				</div>
				
				<div class="filter_elm">
					<label for="city_id">Город:</label>
					<select name="city_id" onchange="this.form.submit();">
						<option value="0"></option>
						<?php
						foreach($cities as $k=>$v){
						?>
						<option value="<?php echo $k; ?>"<?php echo ((isset($pst['city_id'])&&$pst['city_id']==$k)?' selected':''); ?>><?php echo $v; ?></option>
						<?php
						}
						?>
					</select>
				</div>

				<div class="filter_elm">
					<label for="district_id">Район:</label>
					<select name="district_id" onchange="this.form.submit();">
						<option value="0"></option>
						<?php
						foreach($districts as $k=>$v){
						?>
						<option value="<?php echo $k; ?>"<?php echo ((isset($pst['district_id'])&&$pst['district_id']==$k)?' selected':''); ?>><?php echo $v; ?></option>
						<?php
						}
						?>
					</select>
				</div>
			</form>
			<ul id="MainMenu" class="mainmenu">
				<li><?php echo $this->Html->link('Няни', '/nanny/'); ?></li>
				<li><?php echo $this->Html->link('Домработницы', '/cleaner/'); ?></li>
				<li><?php echo $this->Html->link('Сиделки', '/nurse/'); ?></li>
			</ul>
			<!-- <h1><?php //echo dirname(__FILE__).DS.basename(__FILE__); ?></h1> -->
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			&copy;<?php echo date('Y');?>
		</div>
	</div>
	<?php /*echo $this->element('sql_dump');*/
		/*Закомментировать при подключенном DebugKit*/
	?>
</body>
</html>
