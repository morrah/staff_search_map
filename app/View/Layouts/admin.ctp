<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title_for_layout; ?></title>
	<?php
		echo $this->Html->meta('description',!empty($settings["seo_descr"])?$settings["seo_descr"]:"");
		echo $this->Html->meta('keywords',!empty($settings["seo_keywords"])?$settings["seo_keywords"]:"");	
		
		echo $this->Html->meta('icon');

		echo $this->Html->css('style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		if((isset($Fields))&&(in_array('geo_lon',$Fields))){
			echo $this->Html->script('http://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru_RU');
		}
		echo $this->Html->script(array('jquery-1.11.0', 'scripts'));
		echo $this->Html->scriptBlock('
		var MyjQ=new jQuery.noConflict();
		');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container" class="admin">
		<div id="header">
			<h1><?php echo $this->Html->link(__('Администрирование'), 'http://'.$_SERVER['HTTP_HOST'].'/admin'); ?></h1>
			<h2><?php echo $this->Html->link(__('На сайт'), 'http://'.$_SERVER['HTTP_HOST']); ?></h2>
			<ul id="UserMenu" class="usermenu">
				<?php
				if((empty($Auth))||(!isset($Auth['id']))){
				?>
				<li><?php echo $this->Html->link('Вход', '/users/login/'); ?></li>
				<li><?php echo $this->Html->link('Регистрация', '/users/register/'); ?></li>
				<?php
				}
				else{
				?>
				<li><?php echo $this->Html->link('Личный кабинет '.$Auth['username'], '/users/view/'.$Auth['id']); ?></li>
				<li><?php echo $this->Html->link('Выход', '/users/logout/'); ?></li>
				<?php
				}
				?>
			</ul>
			<ul id="MainMenu" class="mainmenu">
				<?php foreach($cntrls as $v){
				?>
					<li><?php echo $this->Html->link(__($v), '/admin/'.$v.'/'); ?></li>
				<?php
				}
				?>
			</ul>
			<!-- <h1><?php //echo dirname(__FILE__).DS.basename(__FILE__); ?></h1> -->
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			&copy;<?php echo date('Y');?>
		</div>
	</div>
	<?php /*echo $this->element('sql_dump');*/
		/*Закомментировать при подключенном DebugKit*/
	?>
</body>
</html>
