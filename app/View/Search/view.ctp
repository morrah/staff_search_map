<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

if (!Configure::read('debug')):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<div id="Center">
	<h2>Соискатель: </h2>
	<table class="fltlt" style="width:auto;">
	<?php foreach($emp["Emp"] as $ek=>$em){echo $this->Html->TableCells(array($ek,$em) );} ?>
	</table>
	<div class="fltlt">
	<h2>Дополнительные поля: </h2>
	<table class="fltlt" style="width:auto;">
	<?php /**/
	foreach($emp["empfieldvalue"] as $ek=>$em){
		?>
		<tr><td><?php echo $em['empfield_name']; ?></td><td><?php echo $em['value']; ?></td></tr>
		<?php
		/*foreach($em as $ekk=>$emm){
		echo $this->Html->TableCells(array($ekk,$emm) );
		}*/
	}
	 ?>
	</table>
	</div>
	<div class="fltrt">
		<h2>Карта: </h2>
		<div id="YMapsID" style="width: 300px; height: 200px;"></div>

		<script type="text/javascript">
			ymaps.ready(init);
			var myMap, myGeoObject;
			function init (ymaps) {
				myMap = new ymaps.Map("YMapsID", {
					center: [<?php echo $emp ["Emp"]['geo_lat'].','.$emp ["Emp"]['geo_lon']; ?>],
					zoom: 10
				});

				myGeoObject = new ymaps.GeoObject({
					geometry: {
					type: "Point",
					coordinates: [<?php echo $emp ["Emp"]['geo_lat'].','.$emp ["Emp"]['geo_lon']; ?>]
				},
				properties: {
					balloonContentBody: '<?php echo $emp ["Emp"]['name']; ?><br><?php echo $emp ["Emp"]['region_id']; ?>'
				}
				});

				myMap.geoObjects.add(myGeoObject);
			}
		</script>
	</div>
</div>

<div id="dbg_nfo" style="display:none;">
	<p>
		<?php /*echo var_dump($emp);*/ ?>
	</p>
</div>