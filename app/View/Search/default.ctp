<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

if (!Configure::read('debug')):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<div id="Center">
	<h2>Карта: </h2>
	<div id="YMapsID" style="width: 100%; height: 500px;"></div>

	<script type="text/javascript">
		ymaps.ready(init);
		var myMap, myGeoObjects, myClusterer;
		function init (ymaps) {
			myMap = new ymaps.Map("YMapsID", {
				center: [54.563921,86.516448],
				zoom: 14
			});
			var myGeoObjects = [];
			
			<?php
			foreach($emps as $k=>$emp){
				?>
				myGeoObjects[<?php echo $k; ?>] = new ymaps.GeoObject({
					geometry: {
					type: "Point",
					coordinates: [<?php echo $emp ["Search"]['geo_lat'].','.$emp ["Search"]['geo_lon']; ?>]
				},
				properties: {
					clusterCaption: ''+(<?php echo $k+1; ?>)+') <?php echo $emp ["Search"]['name']; ?>',
					balloonContentBody: '<?php echo $emp ["Search"]['name']; ?><br><a href="/nanny/<?php echo $emp ["Search"]['id']; ?>" target="_blank"><?php echo $emp ["Search"]['name']; ?></a>'
				}
				});
				<?php
			} ?>

			var myClusterer = new ymaps.Clusterer(
				{clusterDisableClickZoom: true}
			);
			myClusterer.add(myGeoObjects);
			myMap.geoObjects.add(myClusterer);
			myMap.setBounds(myClusterer.getBounds());
			myMap.setZoom(myMap.getZoom()-1);
		}
	</script>
	<script type="text/javascript">
		function toggleblockwidth(selector){
			var blockwidth=jQuery(selector).width();
			if(blockwidth<20){
				blockwidth=200+'px';
			}
			else{
				blockwidth=12+'px';
			}
			jQuery(selector).css({'width':blockwidth});
		}
	</script>
</div>
<div id="LeftColumn">
	<a class="showhideblock fltrt" onclick="toggleblockwidth('div#LeftColumn')"><</a>
	<h2>Фильтры</h2>
	<form class="filter_form" method="POST" action="">
		<div class="filter_elm">
			<label for="region_id">Регион:</label>
			<select name="region_id">
				<option value="0"></option>
				<option value="1"<?php echo ((isset($pst['region_id'])&&$pst['region_id']=='1')?' selected':''); ?>>Новосибирская</option>
				<option value="2"<?php echo ((isset($pst['region_id'])&&$pst['region_id']=='2')?' selected':''); ?>>Кемеровская</option>
			</select>
		</div>
		<div class="filter_elm">
			<label for="city_id">Город:</label>
			<select name="city_id">
				<option value="0"></option>
				<option value="1"<?php echo ((isset($pst['city_id'])&&$pst['city_id']=='1')?' selected':''); ?>>Новокузнецк</option>
				<option value="2"<?php echo ((isset($pst['city_id'])&&$pst['city_id']=='2')?' selected':''); ?>>Кемерово</option>
			</select>
		</div>
		<div class="filter_elm">
			<label for="district_id">Район:</label>
			<select name="district_id">
				<option value="0"></option>
				<option value="1"<?php echo ((isset($pst['district_id'])&&$pst['district_id']=='1')?' selected':''); ?>>Центральный</option>
				<option value="2"<?php echo ((isset($pst['district_id'])&&$pst['district_id']=='2')?' selected':''); ?>>Заводской</option>
			</select>
		</div>
		
		<input type="submit" name="filter_submit" value="Найти" />
	</form>
</div>
<div id="RightColumn">
	<a class="showhideblock fltlt" onclick="toggleblockwidth('div#RightColumn')">></a>
	<h2>Результаты</h2>
	<table>
	<?php foreach($emps as $emp){
		//echo $this->Html->TableCells($emp ["Search"]);
		?>
		<tr>
		<td><?php echo $this->Html->link($emp ["Search"]['name'], /*array('controller'=>'Search', 'action'=>'view', 'id'=>$emp ["Search"]['id'])*/'/nanny/'.$emp ["Search"]['id']); ?></td>
		</tr>
		<?php
	} ?>
	</table>	
</div>
<div id="dbg_nfo" style="display:none;">
	<p>
		$emps=<br />
		<?php echo var_dump($emps); ?>
	</p>
	<p>
		$pst=<br />
		<?php echo var_dump($pst); ?>
	</p>
	<p>
		$cnd=<br />
		<?php echo var_dump($cnd); ?>
	</p>
</div>