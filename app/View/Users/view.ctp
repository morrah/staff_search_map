<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

if (!Configure::read('debug')):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<div id="Center">
	<h2>Пользователь: </h2>
	<table style="width:auto;">
	<?php foreach($item["User"] as $ek=>$em){echo $this->Html->TableCells(array($ek,$em) );} ?>
	</table>
	<?php
	echo $this->Html->link(__("Edit"), array('controller'=>'users','action'=>'edit', $item["User"]['id']));
	?>
	<br />
	<?php
	echo $this->Form->postLink(
			__('Delete'),
			array('action' => 'delete', $item["User"]['id']),
			null,
			__('Are you sure you want to delete # %s?', $item["User"]['id']));//$this->Html->link(__("Delete"), array('controller'=>'users','action'=>'delete', $item["User"]['id']));
	?>
</div>

<div id="dbg_nfo">
	<p>
		<?php /*echo var_dump($item);*/ ?>
	</p>
</div>