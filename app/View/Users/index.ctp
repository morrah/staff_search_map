<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

if (!Configure::read('debug')):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<div id="Center">
	<h2>Пользователи: </h2>
	<table class="fltlt" style="width:auto;">
	<?php foreach($items as $item){
		foreach($item["User"] as $ek=>$em){echo $this->Html->TableCells(array($ek,$em) );}
			}?>
	</table>
</div>

<div id="dbg_nfo">
	<p>
		<?php /*echo var_dump($items);*/ ?>
	</p>
</div>