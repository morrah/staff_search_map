<?php
App::uses('AppModel', 'Model');
class Emp extends AppModel {
	public $belongsTo = array(
		'Spec' => array(
			'className' => 'Spec',
			'foreign_key' => 'spec_id'
		),
		'User' => array(
			'className' => 'User',
			'foreign_key' => 'user_id'
		),
		'Country' => array(
			'className' => 'Country',
			'foreign_key' => 'country_id'
		),
		'Region' => array(
			'className' => 'Region',
			'foreign_key' => 'region_id'
		),
		'City' => array(
			'className' => 'City',
			'foreign_key' => 'city_id',
		),
		'District' => array(
			'className' => 'District',
			'foreign_key' => 'district_id',
		),
		'Nation' => array(
			'className' => 'Nation',
			'foreign_key' => 'nation_id',
		)
	);
	public $hasMany = array(
		'empfieldvalue' => array(
			'className' => 'Empfieldvalue',
			'foreign_key' => 'emp_id',
			'dependent' => true
		)
	);	
}