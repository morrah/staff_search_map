<?php
App::uses('AppModel', 'Model');
class District extends AppModel {
	public $belongsTo = array(
		'City' => array(
			'className' => 'City',
			'foreign_key' => 'city_id'
		)
	);
}