<?php
App::uses('AppModel', 'Model');
class Permission extends AppModel {
	 public $hasAndBelongsToMany = array(
        'PermissionGroups' =>
            array(
                'className' => 'Group',
                'joinTable' => 'groups_permissions',
                'foreignKey' => 'permission_id',
                'associationForeignKey' => 'group_id',
                'unique' => true
            )	
    );
}