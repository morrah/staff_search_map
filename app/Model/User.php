<?php
/**
 * Users model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppModel', 'Model');

/**
 * Users model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class User extends AppModel {
    public $name = 'User';
/**
 * Table name for this Model.
 *
 * @var string
 */
	//public $table = 'Search';
	
/**
 * Custom database table name, or null/false if no table association is desired.
 *
 * @var string
 * @link http://book.cakephp.org/2.0/en/models/model-attributes.html#usetable
 */
	public $useTable = 'users';

/**
 * The name of the primary key field for this model.
 *
 * @var string
 * @link http://book.cakephp.org/2.0/en/models/model-attributes.html#primaryKey
 */
	public $primaryKey = 'id';
	
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            )
        ),
        'email_address' => array(
            'required' => array(
                'rule' => array('email'),
                'message' => 'E-mail is not valid'
			)
        )
    );	
	
	public $hasMany = array(
		'UserEmps' => array(
			'className' => 'Emp',
			'foreign_key' => 'user_id',
			'dependent' => true
		),
		'UserVacancies' => array(
			'className' => 'Vacancy',
			'foreign_key' => 'user_id',
			'dependent' => true
		)
	);
	public $hasAndBelongsToMany = array(
        'UserGroups' =>
            array(
                'className' => 'Group',
                'joinTable' => 'groups_users',
                'foreignKey' => 'user_id',
                'associationForeignKey' => 'group_id',
                'unique' => true/*,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => ''*/
            ),
        'UserServices' =>
            array(
                'className' => 'Service',
                'joinTable' => 'services_users',
                'foreignKey' => 'user_id',
                'associationForeignKey' => 'service_id',
                'unique' => true/*,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => ''*/
            )
    );
	public function beforeSave() {
		$this->data[$this->alias]['password'] = Security::hash($this->data[$this->alias]['password'] , null, true);
	}

 
}