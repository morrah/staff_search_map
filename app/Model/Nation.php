<?php
App::uses('AppModel', 'Model');
class Nation extends AppModel {
	public $hasMany = array(
		'emp' => array(
			'className' => 'Emp',
			'foreign_key' => 'nation_id',
			'dependent' => false
		),
		'vacancy' => array(
			'className' => 'Vacancy',
			'foreign_key' => 'nation_id',
			'dependent' => false
		)
	);
}