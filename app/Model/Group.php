<?php
App::uses('AppModel', 'Model');
class Group extends AppModel {
	 public $hasAndBelongsToMany = array(
        'GroupPermissions' =>
            array(
                'className' => 'Permission',
                'joinTable' => 'groups_permissions',
                'foreignKey' => 'group_id',
                'associationForeignKey' => 'permission_id',
                'unique' => true
            ),
        'GroupUsers' =>
            array(
                'className' => 'User',
                'joinTable' => 'groups_users',
                'foreignKey' => 'group_id',
                'associationForeignKey' => 'user_id',
                'unique' => true
            )			
    );
}