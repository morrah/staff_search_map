<?php
App::uses('AppModel', 'Model');
class Empfield extends AppModel {
	public $belongsTo = array(
		'Spec' => array(
			'className' => 'Spec',
			'foreign_key' => 'spec_id'
		)
	);
	public $hasMany = array(
		'empfieldvalue' => array(
			'className' => 'Empfieldvalue',
			'foreign_key' => 'empfield_id',
			'dependent' => true
		)
	);	
}