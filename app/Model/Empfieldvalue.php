<?php
App::uses('AppModel', 'Model');
class Empfieldvalue extends AppModel {
	public $belongsTo = array(
		'empfield' => array(
			'className' => 'Empfield',
			'foreign_key' => 'empfield_id'
		),
		'emp' => array(
			'className' => 'Emp',
			'foreign_key' => 'emp_id'
		)
	);	
}