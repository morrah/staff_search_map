<?php
App::uses('AppModel', 'Model');
class Country extends AppModel {
	public $hasMany = array(
		'Region' => array(
			'className' => 'Region',
			'foreign_key' => 'country_id',
			'dependent' => true
		)
	);
}