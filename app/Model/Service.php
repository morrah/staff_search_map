<?php
App::uses('AppModel', 'Model');
class Service extends AppModel {
	public $hasAndBelongsToMany = array(
        'ServiсeUsers' =>
            array(
                'className' => 'User',
                'joinTable' => 'services_users',
                'foreignKey' => 'service_id',
                'associationForeignKey' => 'user_id',
                'unique' => true/*,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => ''*/
            )
    );
}