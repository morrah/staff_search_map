<?php
App::uses('AppModel', 'Model');
class City extends AppModel {
	public $belongsTo = array(
		'Region' => array(
			'className' => 'Region',
			'foreign_key' => 'region_id'
		)
	);
	public $hasMany = array(
		'District' => array(
			'className' => 'District',
			'foreign_key' => 'city_id',
			'dependent' => true
		)
	);
}