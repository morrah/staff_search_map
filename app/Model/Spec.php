<?php
App::uses('AppModel', 'Model');
class Spec extends AppModel {
	public $useTable='spec';
	public $hasMany = array(
		'emp' => array(
			'className' => 'Emp',
			'foreign_key' => 'spec_id',
			'dependent' => true
		),
		'empfield' => array(
			'className' => 'Empfield',
			'foreign_key' => 'spec_id',
			'dependent' => true
		)
	);
}