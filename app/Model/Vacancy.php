<?php
App::uses('AppModel', 'Model');
class Vacancy extends AppModel {
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreign_key' => 'user_id'
		),
		'Spec' => array(
			'className' => 'Spec',
			'foreign_key' => 'spec_id'
		),
		'Region' => array(
			'className' => 'Region',
			'foreign_key' => 'region_id'
		),
		'City' => array(
			'className' => 'City',
			'foreign_key' => 'city_id'
		),
		'District' => array(
			'className' => 'District',
			'foreign_key' => 'district_id'
		)
	);
}