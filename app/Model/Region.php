<?php
App::uses('AppModel', 'Model');
class Region extends AppModel {
	public $belongsTo = array(
		'Country' => array(
			'className' => 'Country',
			'foreign_key' => 'country_id'
		)
	);
	public $hasMany = array(
		'City' => array(
			'className' => 'City',
			'foreign_key' => 'region_id',
			'dependent' => true
		)
	);
}