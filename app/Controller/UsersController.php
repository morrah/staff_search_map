<?php
App::uses('AppController', 'Controller');
//App::uses('CakeSession', 'Model/Datasource');
/**
 * Users controller
 *
 */class UsersController extends AppController {


/**
 * This controller use a model
 *
 * @var array
 */
	public $uses = array('User');
	
	//public $helpers = array('Html', 'Form');
	
	//public $scaffold;
	
	/*public function beforeFilter(){
		parent::beforeFilter();
		if((($this->request->action=='view')||($this->request->action=='edit')||($this->request->action=='delete'))&&($this->request->pass[0]==$this->Auth->user('id'))){
			
			$this->Auth->allow(array('view','edit','delete'));
		}
		$this->Auth->allow($this->methods);
		$this->Auth->allow(array('index','display','view','login','edit','delete'));
		if(mb_substr($this->request->action,0,6,'UTF-8')=='admin'){
			$this->request->action=mb_substr($this->request->action,6,mb_strlen($this->request->action,'UTF-8'),'UTF-8');
		}
	}*/
    public function login(){
	    if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				//return $this->redirect($this->Auth->redirectUrl());
				// Prior to 2.3 use
				$this->Session->setFlash(__('User %s is logged in',$this->Auth->user('username')));
				return $this->redirect($this->Auth->redirect());
			} /*else {
				$this->Session->setFlash(
					__('Username or password is incorrect'),
					'default',
					array(),
					'auth'
				);
			}*/
			$this->Session->setFlash(__('Invalid username or password, try again'));
		}
	}
    public function admin_login(){
		$this->set('View', 'login');
		$this->login();
	}
    public function logout(){
        $this->Session->delete('Permissions');
		$this->Session->delete('thisGroups');
		$this->Session->delete('thisGroups00');
        $this->redirect($this->Auth->logout());
    }	
	
	public function register() {
		$this->Auth->allow();
		if ($this->request->is('post')) {
			//if ($this->User->save($this->request->data)) {
			if($this->_scaffoldSave($this->request, 'add')){
				$this->Session->setFlash(__('The user has been saved'));
				$id = $this->User->id;
				$this->request->data['User'] = array_merge(
					$this->request->data['User'],
					array('id' => $id)
				);
				$this->Auth->login($this->request->data['User']);
				return $this->redirect('/users/edit/'.$id);
			}
			$this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
		}
	}
	
	public function edit(){
		$Fields=array_keys($this->{$this->modelClass}->schema());
		$this->set('Fields',$Fields);
		if (isset($this->request->params['pass'][0])) {
					$this->{$this->modelClass}->id = $this->request['pass'][0];
				}
				//if (!$this->{$this->modelClass}->exists()) {
					//throw new NotFoundException(__d('cake', 'Invalid %s', Inflector::humanize($this->modelKey)));
				//}
				if ($this->{$this->modelClass}->id) {
					$this->data = $this->request->data = $this->{$this->modelClass}->read();
				}
		if ($this->request->is('post')) {
			//if ($this->User->save($this->request->data)) {
			if($this->_scaffoldSave($this->request, 'add')){
				$this->Session->setFlash(__('The user has been updated'));
				$id = $this->User->id;
				$this->request->data['User'] = array_merge(
					$this->request->data['User'],
					array('id' => $id)
				);
				$this->Auth->login($this->request->data['User']);
				return $this->redirect('/users/view/'.$id);
			}
			$this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
		}
	}

		public function delete(){
		//if ($this->request->is('post')) {
			//if ($this->User->delete($this->request->pass[0])) {
			if($this->_scaffoldDelete($this->request)){
				$this->Session->setFlash(__('The user has been deleted'));
				$this->Auth->logout();
				return $this->redirect('/');
			}
			$this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
		//}
	}
	
	public function confirm(){
	
	}
	
	public function pass_recovery(){
	
	}
	
	public function profile($id){
		$this->view($id);
	}	
	
	public function view($id=null) {
		$id=$this->request->pass['0'];
		if (empty($id)) {
			return $this->redirect('/');
		}
		$this->set('item', $this->User->find('first',array('conditions'=>array('id'=>$id))));
	}
	
	public function index() {
		$this->set('items', $this->User->find('all',array('conditions'=>$this->filter(array()))));
		try {
			$this->render();
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
    }
	
	private function filter($conditions=array()){
		if(empty($conditions)||!is_array($conditions)){
			$conditions=array();
		}
		$fields=array('id','name');
		if(!empty($this->request->data)){
			foreach($fields as $field){
				if(!empty($this->request->data[$field])){
					$conditions[$field]=(string)$this->request->data($field);
				}
			}
		}
		$this->set('cnd', $conditions);		
		return $conditions;
	}
}