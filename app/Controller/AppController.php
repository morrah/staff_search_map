<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
//App::uses('CakeSession', 'Model/Datasource');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Session'
		,'Auth'/*=>array('allowedActions'=>array('*','display','view','login','edit','delete'))*/
		,'Paginator'
		,'DebugKit.Toolbar'/**/
		/*Раскомментировать при подключенном DebugKit*/
	
		
	);
	public $helpers = array('Html', 'Form', 'Session');
	
	public $controllerList = array();
	
	public $cntrls = array('Users','Emps','Empfields','Empfieldvalues','Vacancies','Services','Countries','Regions','Cities','Districts','Groups','Permissions','Nations','Specs','Settings');
	/**
 * Valid session.
 *
 * @var boolean
 */
	protected $_validSession = null;
    /**
     * beforeFilter
     * 
     * Это событие обрабатывается перед любым действием контроллера
     * 
     * @access public 
     */
    public function beforeFilter(){
        //Устанавливаем поля для авторизации в компоненте Auth вместо тех, что идут по умолчанию
        //$this->Auth->fields = array('username'=>'login','password'=>'pass');
        // Устанавливаем действия, доступные без авторизации по всей системе
		//$this->allowedActions = $this->_methods;
        //$this->Auth->allow($this->methods);
		//$this->Auth->allow(array('*','index','display','view','login','logout','add','edit','delete'));
		$this->Auth->allow(array('display','login','logout','register'));//В частности указываем, что на статические страницы /pages/* доступ будет открыт всегда (к примеру, для отображения главной)
		
		if(($this->request->controller=='users')&&(!empty($this->request->pass[0]))&&($this->request->pass[0]==$this->Auth->user('id'))){
			
			$this->Auth->allow(array('view','edit','delete','login'));
		}
		
        //Страница, на которую будут переходить пользователи после выхода из системы
        $this->Auth->logoutRedirect = '/';
        //Страница, на которую будет переходить пользователь после входа в систему
        $this->Auth->loginRedirect = '/';
        //Расширим компонент Auth при помощи действия isAuthorized
        $this->Auth->authorize = array('Controller');
        //Разрешим доступ только тем пользователям чьи профили активны
        $this->Auth->userScope = array('User.enabled = 1');
        //Передаём компонент авторизации в страницы вида
        $this->set('Auth',$this->Auth->user());	
		$this->controllerList = App::objects('Controller');//Configure::read('controller');
		$this->set('controllerList',$this->controllerList);
		
		//Загружаем настройки из базы
		$this->loadModel('Setting');
		$sets=$this->Setting->find('all',array('conditions'=>array('enabled'=>'1')));//;
		//$this->set('sets', $sets);
		$settings=array();
		foreach($sets as $k=>$v){
			$settings[$v["Setting"]["name"]]=$v["Setting"]["value"];
		}
		$this->set('settings', $settings);
		if(!empty($settings["seo_title"])){
			$this->set('title_for_layout',$settings["seo_title"]);
		}
		//Проверяем наличие сессии
		$this->_validSession = (
			isset($this->Session) && $this->Session->valid()
		);
		//Выводим фильтры в шаблон, если не в админке
		if(!($this->request->prefix=='admin')){
			//Загружаем страны из базы
			$this->loadModel('Country');
			$sets=$this->Country->find('all',array('conditions'=>array('enabled'=>'1')));//;
			//$this->set('sets', $sets);
			$countries=array();
			foreach($sets as $k=>$v){
				$countries[$v["Country"]["id"]]=$v["Country"]["name"];
			}
			$this->set('countries', $countries);
			if((!empty($this->request->data))&&(!empty($this->request->data['country_id']))){
				$default_country_id=$this->request->data['country_id'];
			}
			elseif(isset($settings['default_country_id'])){
				$default_country_id=$settings['default_country_id'];
			}
			else{
				$default_country_id=1;
			}
			$this->set('default_country_id', $default_country_id);
			
			//Загружаем регионы из базы
			$this->loadModel('Region');
			$sets=$this->Region->find('all',array('conditions'=>array('Region.enabled'=>'1','country_id'=>$default_country_id)));//;
			//$this->set('sets', $sets);
			$regions=array();
			foreach($sets as $k=>$v){
				$regions[$v["Region"]["id"]]=$v["Region"]["name"];
			}
			$this->set('regions', $regions);
			if((!empty($this->request->data))&&(!empty($this->request->data['region_id']))){
				$default_region_id=$this->request->data['region_id'];
			}
			elseif(isset($settings['default_region_id'])){
				$default_region_id=$settings['default_region_id'];
			}
			else{
				$default_region_id=1;
			}
			$this->set('default_region_id', $default_region_id);
			
			//Загружаем города из базы
			$this->loadModel('City');
			$sets=$this->City->find('all',array('conditions'=>array('City.enabled'=>'1','region_id'=>$default_region_id)));//;
			//$this->set('sets', $sets);
			$cities=array();
			foreach($sets as $k=>$v){
				$cities[$v["City"]["id"]]=$v["City"]["name"];
			}
			$this->set('cities', $cities);
			if((!empty($this->request->data))&&(!empty($this->request->data['city_id']))){
				$default_city_id=$this->request->data['city_id'];
			}
			elseif(isset($settings['default_city_id'])){
				$default_city_id=$settings['default_city_id'];
			}
			else{
				$default_city_id=1;
			}
			$this->set('default_city_id', $default_city_id);
			
			//Загружаем районы города из базы
			$this->loadModel('District');
			$sets=$this->District->find('all',array('conditions'=>array('District.enabled'=>'1','city_id'=>$default_city_id)));//;
			//$this->set('sets', $sets);
			$districts=array();
			foreach($sets as $k=>$v){
				$districts[$v["District"]["id"]]=$v["District"]["name"];
			}
			$this->set('districts', $districts);
		}
    }
    /**
     * beforeRender
     * 
     * Это событие происходит перед тем, как страница отрисовывается
     *
     * 
     * @access public 
     */
    public function beforeRender(){
        //Если пользователь авторизирован, то мы обрабатываем
        //список разрешенных для него действий

        if($this->Auth->user()){
            $controllerList = App::objects('Controller');//Configure::read('controller');
			$this->set('controllerList',$controllerList);
            $permittedControllers = array();
            foreach($controllerList as $controllerItem){
                if($controllerItem <> 'App'){
                    if($this->__permitted($controllerItem,'index')){
                        $permittedControllers[] = $controllerItem;
                    }
                }
            }
        }
        $this->set(compact('permittedControllers'));

    }
    /**
     * isAuthorized
     * 
     * Вызывается компонентом Auth для проверки доступа к элементу
     * тут мы и будем проводить нашу проверку
     * 
     * @return true if authorised/false if not authorized
     * @access public
     */
    public function isAuthorized(){
        return $this->__permitted($this->name,$this->action);
    }
    /**
     * __permitted
     * 
     * Вспомогательная функция, которая производит проверку прав пользователя
     * описанных в форме $controllerName:$actionName
     * @return 
     * @param $controllerName Object
     * @param $actionName Object
     */
    function __permitted($controllerName,$actionName){
        //Имя контроллера указываем в нижнем регистре
        $controllerName = strtolower($controllerName);
        $actionName = strtolower($actionName);
			//$this->Session->write('controllerName',$controllerName);
			//$this->Session->write('actionName',$actionName);		
        //Если в сессии права не были закешированы
        if(/*(!$this->Session)||*/(!$this->Session->check('Permissions'))){
            //...то подготовим массив для сохранения
            $permissions = array();
            //у всех есть право выйти из системы
            $permissions[]='users:logout';
            //Импортируем модель пользователя, чтобы получить права
            $this->loadModel('User',$this->Auth->user('id'));
            $thisUser = new User;
            //Получаем текущего пользователя и его группу
            //$thisGroups = $thisUser->find(array('User.id'=>$this->Auth->user('id')));
            
			$thisGroups = $this->User->find('first',array('conditions'=>array('id'=>$this->Auth->user('id'))));
$this->Session->write('thisGroups00',$thisGroups);
			
			$thisGroups = $thisGroups['UserGroups'];
			$this->Session->write('thisGroups',$thisGroups);
            foreach($thisGroups as $thisGroup){
                $thisGPermissions = $thisUser->UserGroups->find('all',array('conditions'=>array('id'=>$thisGroup['id'])));
				//$this->Session->write('thisGPermissions',$thisGPermissions);
				
				foreach($thisGPermissions as $thisGPermissionKey=>$thisGPermission){
					$thisPermissions = $thisGPermission['GroupPermissions'];
					//$this->Session->write('thisPermissions.'.$thisGPermissionKey,$thisPermissions);
					foreach($thisPermissions as $thisPermissionKey=>$thisPermission){
						$permissions[]=$thisPermission['name'];
					}
				}
            }
			/*if(!$this->Session){
				$this->Session = new Session;
			}*/
            //Записываем права в сессию
            $this->Session->write('Permissions',$permissions);
        }
		else{
            //...видимо права закешированы, загружаем из сессии
            $permissions = $this->Session->read('Permissions');
        }
		
        //Ищем среди прав соотвествующее текущему
        foreach($permissions as $permission){
            if($permission == '*'){
                return true;//Найдено право СуперАдмина :)
            }
            if($permission == $controllerName.':*'){
                return true;//Разрешаются все действия в данном контроллере
            }
            if($permission == $controllerName.':'.$actionName){
                return true;//Найдено определённое действие
            }
        }
        return false;
    }
	
	public function admin_index(){
		$singularVar = Inflector::variable($this->modelClass);
		$pluralVar = Inflector::variable($this->name);
		$modelClass = $this->modelClass;
		$primaryKey = $this->{$this->modelClass}->primaryKey;
		$Fields=array_keys($this->{$this->modelClass}->schema());
		$data=$this->paginate();
		$associations = $this->_associations();
		$cntrls = $this->cntrls;//array('Users','Emps','Vacancies','Services','Regions','Cities','Districts','Groups','Permissions','Nations','Specs','Settings');
		$this->set(compact('singularVar', 'pluralVar','Fields', 'data', 'associations', 'modelClass', 'primaryKey', 'cntrls'));
		/*$this->_validSession = (
			isset($this->Session) && $this->Session->valid()
		);*/
		$this->layout='admin';
		$this->render('/Admin/admin_index');
	}
	
	public function admin_view(){
		$singularVar = Inflector::variable($this->modelClass);
		$pluralVar = Inflector::variable($this->name);
		$modelClass = $this->modelClass;
		$primaryKey = $this->{$this->modelClass}->primaryKey;
		$Fields=array_keys($this->{$this->modelClass}->schema());
		//$data=$this->paginate();
		$associations = $this->_associations();
		$cntrls = $this->cntrls;//array('Users','Emps','Vacancies','Services','Regions','Cities','Districts','Groups','Permissions','Nations','Specs','Settings');

		if (isset($this->request->params['pass'][0])) {
			$this->{$this->modelClass}->id = $this->request->params['pass'][0];
		}
		if (!$this->{$this->modelClass}->exists()) {
			throw new NotFoundException(__('Invalid %s', Inflector::humanize($this->modelKey)));
		}
		$this->{$this->modelClass}->recursive = 1;
		$this->request->data = $this->{$this->modelClass}->read();
		$this->set(
			Inflector::variable($this->modelClass), $this->request->data
		);
		
		$this->set(compact(
			'singularVar', 'pluralVar', 'Fields', 'associations', 'modelClass', 'primaryKey', 'cntrls'
		));
		/*$this->_validSession = (
			isset($this->Session) && $this->Session->valid()
		);*/
		$this->layout='admin';
		$this->render('/Admin/admin_view');
	}
	
	public function admin_add(){
		$this->_scaffoldSave($this->request, 'add');
	}
	
	public function admin_edit(){
		$this->_scaffoldSave($this->request, 'edit');
	}
	
	public function admin_delete(){
		$this->_scaffoldDelete($this->request);
	}
	
	
	
	/**
 * Renders an add or edit action for scaffolded model.
 *
 * @param string $action Action (add or edit)
 * @return void
 */
	protected function _scaffoldForm($action = 'edit') {
			
		$singularVar = Inflector::variable($this->modelClass);
		$pluralVar = Inflector::variable($this->name);
		$modelClass = $this->modelClass;
		$primaryKey = $this->{$this->modelClass}->primaryKey;
		$associations = $this->_associations();
		$Fields=array_keys($this->{$this->modelClass}->schema());	
		$cntrls = $this->cntrls;//array('Users','Emps','Vacancies','Services','Regions','Cities','Districts','Groups','Permissions','Nations','Specs','Settings');
				$this->set(compact(
			'singularVar', 'pluralVar',
			'Fields', 'associations', 'modelClass', 'primaryKey', 'cntrls'
		));
		$this->viewVars['Fields'] = array_merge(
			/*$this->viewVars['Fields'],*/
			$Fields,
			array_keys($this->{$this->modelClass}->hasAndBelongsToMany)
		);
		/*$this->_validSession = (
			isset($this->Session) && $this->Session->valid()
		);*/
		$this->layout='admin';
		$this->render('/Admin/admin_form', $this->layout);
	}

/**
 * Saves or updates the scaffolded model.
 *
 * @param CakeRequest $request Request Object for scaffolding
 * @param string $action add or edit
 * @return mixed Success on save/update, add/edit form if data is empty or error if save or update fails
 * @throws NotFoundException
 */
	protected function _scaffoldSave(CakeRequest $request, $action = 'edit') {

		$formAction = 'edit';
		$success = __d('cake', 'updated');
		if ($action === 'add') {
			$formAction = 'add';
			$success = __d('cake', 'saved');
		}

		if ($this->beforeScaffold($action)) {
			if ($action === 'edit') {
				if (isset($request->params['pass'][0])) {
					$this->{$this->modelClass}->id = $request['pass'][0];
				}
				if (!$this->{$this->modelClass}->exists()) {
					throw new NotFoundException(__d('cake', 'Invalid %s', Inflector::humanize($this->modelKey)));
				}
			}

			if (!empty($request->data)) {
				if ($action === 'create') {
					$this->{$this->modelClass}->create();
				}

				if ($this->{$this->modelClass}->save($request->data)) {
					if ($this->afterScaffoldSave($action)) {
						$message = __d('cake',
							'The %1$s has been %2$s',
							Inflector::humanize($this->modelKey),
							$success
						);
						return $this->_sendMessage($message);
					}
					return $this->afterScaffoldSaveError($action);
				}
				if ($this->_validSession) {
					$this->Session->setFlash(__d('cake', 'Please correct errors below.'));
				}
			}

			if (empty($request->data)) {
				if ($this->{$this->modelClass}->id) {
					$this->data = $request->data = $this->{$this->modelClass}->read();
				} else {
					$this->data = $request->data = $this->{$this->modelClass}->create();
				}
			}

			foreach ($this->{$this->modelClass}->belongsTo as $assocName => $assocData) {
				$varName = Inflector::variable(Inflector::pluralize(
					preg_replace('/(?:_id)$/', '', $assocData['foreignKey'])
				));
				$this->set($varName, $this->{$this->modelClass}->{$assocName}->find('list'));
			}
			foreach ($this->{$this->modelClass}->hasAndBelongsToMany as $assocName => $assocData) {
				$varName = Inflector::variable(Inflector::pluralize($assocName));
				$this->set($varName, $this->{$this->modelClass}->{$assocName}->find('list'));
			}

			return $this->_scaffoldForm($formAction);
		} elseif ($this->scaffoldError($action) === false) {
			return $this->_scaffoldError();
		}
	}

/**
 * Performs a delete on given scaffolded Model.
 *
 * @param CakeRequest $request Request for scaffolding
 * @return mixed Success on delete, error if delete fails
 * @throws MethodNotAllowedException When HTTP method is not a DELETE
 * @throws NotFoundException When id being deleted does not exist.
 */
	protected function _scaffoldDelete(CakeRequest $request) {
		$singularVar = Inflector::variable($this->modelClass);
		$pluralVar = Inflector::variable($this->name);
		$modelClass = $this->modelClass;
		$primaryKey = $this->{$this->modelClass}->primaryKey;
		$Fields=array_keys($this->{$this->modelClass}->schema());
		$associations = $this->_associations();
		$cntrls = $this->cntrls;//array('Users','Emps','Vacancies','Services','Regions','Cities','Districts','Groups','Permissions','Nations','Specs','Settings');
		if ($this->beforeScaffold('delete')) {
			if (!$request->is('post')) {
				throw new MethodNotAllowedException();
			}
			$id = false;
			if (isset($request->params['pass'][0])) {
				$id = $request->params['pass'][0];
			}
			$this->{$this->modelClass}->id = $id;
			if (!$this->{$this->modelClass}->exists()) {
				throw new NotFoundException(__d('cake', 'Invalid %s', Inflector::humanize($this->modelClass)));
			}
			if ($this->{$this->modelClass}->delete()) {
				$message = __d('cake', 'The %1$s with id: %2$s has been deleted.', Inflector::humanize($this->modelClass), $id);
				return $this->_sendMessage($message);
			}
			$message = __d('cake',
				'There was an error deleting the %1$s with id: %2$s',
				Inflector::humanize($this->modelClass),
				$id
			);
			return $this->_sendMessage($message);
		} elseif ($this->scaffoldError('delete') === false) {
			return $this->_scaffoldError();
		}
	}

/**
 * Sends a message to the user. Either uses Sessions or flash messages depending
 * on the availability of a session
 *
 * @param string $message Message to display
 * @return void
 */
	protected function _sendMessage($message) {
		if ($this->_validSession) {
			$this->Session->setFlash($message);
			//return $this->redirect($this->redirect);
			return $this->redirect(array('controller' => $this->request->controller, 'action' => 'index'));
		}
		$this->flash($message, $this->redirect);
	}

/**
 * Show a scaffold error
 *
 * @return mixed A rendered view showing the error
 */
	protected function _scaffoldError() {
		return $this->render('error', $this->layout);
	}
	
	
/**
 * Returns associations for controllers models.
 *
 * @return array Associations for model
 */
	protected function _associations() {
		$keys = array('belongsTo', 'hasOne', 'hasMany', 'hasAndBelongsToMany');
		$associations = array();

		foreach ($keys as $type) {
			foreach ($this->{$this->modelClass}->{$type} as $assocKey => $assocData) {
				$associations[$type][$assocKey]['primaryKey'] =
					$this->{$this->modelClass}->{$assocKey}->primaryKey;

				$associations[$type][$assocKey]['displayField'] =
					$this->{$this->modelClass}->{$assocKey}->displayField;

				$associations[$type][$assocKey]['foreignKey'] =
					$assocData['foreignKey'];

				list($plugin, $model) = pluginSplit($assocData['className']);
				if ($plugin) {
					$plugin = Inflector::underscore($plugin);
				}
				$associations[$type][$assocKey]['plugin'] = $plugin;

				$associations[$type][$assocKey]['controller'] =
					Inflector::pluralize(Inflector::underscore($model));

				if ($type === 'hasAndBelongsToMany') {
					$associations[$type][$assocKey]['with'] = $assocData['with'];
				}
			}
		}
		return $associations;
	}	
}
