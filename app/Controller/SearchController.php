<?php
/**
 * Search controller.
 *
 * This file will render views from views/search/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Search controller
 *
 */
class SearchController extends AppController {

/**
 * This controller use a model
 *
 * @var array
 */
	public $uses = array('Search');
	
	//public $helpers = array('Html', 'Form');
	
	public function beforeFilter(){
        parent::beforeFilter();
		$this->Auth->allow(array('display','index','nannies','cleaners','nurses'));
		//$this->set('title_for_layout',(!empty($this->title_for_layout)?$this->title_for_layout.' | ':'').$this->request->action);
	}

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	
	private function _check_services(){
		//if(floatval($this->Auth->user('balance'))>0){
			return true;
		//}
		//return false;
	}
	
	public function view() {
		if(!($this->_check_services())){
			$this->Session->setFlash(__('You have not enough rights to access this profile'));
			return $this->redirect('/');
		}
		//$id=isset($this->request->params['id'])?$this->request->params['id']:$this->request->params[0];
		$id=$this->params['id'];
		if (empty($id)) {
			return $this->redirect('/');
		}
		$this->loadModel('Emp');
		$emp=$this->Emp->find('first',array('conditions'=>array('Emp.id'=>$id)));
		$this->loadModel('Empfield');
		$empfields=$this->Empfield->find('all',array('conditions'=>array('Empfield.spec_id'=>$emp['Emp']['spec_id']),'recursive'=>0));
		$empfs=array();
		foreach($empfields as $k=>$v){
			$empfs[$v['Empfield']['id']]=$v['Empfield'];
		}
		$empfields=$empfs;
		$emp['empfields']=$empfields;
		foreach($emp['empfieldvalue'] as $k=>$v){
			$emp['empfieldvalue'][$k]['empfield_name']=$empfields[$v['empfield_id']]['name'];
			$emp['empfieldvalue'][$k]['empfield_type']=$empfields[$v['empfield_id']]['type'];
		}
		$this->set('emp', $emp);
	}
	
	public function index() {
		$this->set('View', 'default');
		/*$conditions=array();
		$fields=array('id','name','region','city','district');
		if(!empty($this->request->data)){
			foreach($fields as $field){
				if(!empty($this->request->data[$field])){
					$conditions[$field]=(string)$this->request->data($field);
				}
			}
		}
		if(!empty($conditions)){
			$this->set('emps', $this->Search->find('all',array('conditions'=>$conditions)));
		}*/
		//else{
			//$this->set('emps', $this->Search->find('all'),array());
			$this->set('emps', $this->Search->find('all',array('conditions'=>$this->filter(array()))));
			//$this->filter();
		//}
		$this->set('pst', $this->request->data);

		try {
			$this->render('default');
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
		//$this->display();
    }
	
	public function nannies(){
		$this->Auth->allow();
		$this->set('View', 'default');
		$this->set('emps', $this->Search->find('all',array('conditions'=>$this->filter(array('spec_id'=>'1')))));
		$this->set('pst', $this->request->data);		
		try {
			$this->render('default');
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
		//$this->display();
	}
	public function cleaners(){
		$this->Auth->allow();
		$this->set('View', 'default');
		$this->set('emps', $this->Search->find('all',array('conditions'=>$this->filter(array('spec_id'=>'2')))));
		$this->set('pst', $this->request->data);		
		try {
			$this->render('default');
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
		//$this->display();
	}
	public function nurses(){
		$this->Auth->allow();
		$this->set('View', 'default');
		$this->set('emps', $this->Search->find('all',array('conditions'=>$this->filter(array('spec_id'=>'3')))));
		$this->set('pst', $this->request->data);		
		try {
			$this->render('default');
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
		//$this->display();
	}
	
	public function admin_index() {
		$adminControllers=array();
		$controllerList = $this->controllerList;/*App::objects('Controller');*/
		foreach($controllerList as $controllerItem){
			if($controllerItem <> 'AppController'){
				$controllerMethods = get_class_methods($controllerItem);
				if(is_array($controllerMethods)){
					foreach($controllerMethods as $controllerMethod){
						//if(mb_substr($controllerMethod,0,6,'UTF-8')=='admin_'){
							$adminControllers[$controllerItem][] = $controllerMethod;
						//}
					}
				}
			}
		}
		$this->set('adminControllers', $adminControllers);
		//$this->set('View', 'default');
		/*$conditions=array();
		$fields=array('id','name','region','city','district');
		if(!empty($this->request->data)){
			foreach($fields as $field){
				if(!empty($this->request->data[$field])){
					$conditions[$field]=(string)$this->request->data($field);
				}
			}
		}
		if(!empty($conditions)){
			$this->set('emps', $this->Search->find('all',array('conditions'=>$conditions)));
		}*/
		//else{
			//$this->set('emps', $this->Search->find('all'),array());
			$this->set('emps', $this->Search->find('all',array('conditions'=>$this->filter(array()))));
			//$this->filter();
		//}
		$this->set('pst', $this->request->data);
		$cntrls = $this->cntrls;//array('Users','Emps','Empfields','Empfieldvalues','Vacancies','Services','Countries','Regions','Cities','Districts','Groups','Permissions','Nations','Specs','Settings');
		$this->set('cntrls', $cntrls);
		$this->layout='admin';
		try {
			$this->render();
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
		//$this->display();
    }
	
	public function admin_emps() {
		$this->set('emps', $this->Search->find('all',array('conditions'=>$this->filter(array()))));
		try {
			$this->render();
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	
	private function filter($conditions=array()){
		if(empty($conditions)||!is_array($conditions)){
			$conditions=array();
		}
		$fields=array('id','name','country_id','region_id','city_id','district_id');
		if(!empty($this->request->data)){
			foreach($fields as $field){
				if(!empty($this->request->data[$field])){
					$conditions[$field]=(string)$this->request->data($field);
				}/**/
			}
		}
		$this->set('cnd', $conditions);		
		return $conditions;
	}
}
