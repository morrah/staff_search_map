<?
App::uses('AppController', 'Controller');
/**
 * My controller
 *
 */
class MyController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
	public $scaffold;
	/*public function index() {
		$this->View="default";
        $this->set('emps', 'my emps');
		//$this->display();
    }
	public function boots() {
        $this->set('emps', 'my boots');
		//$this->display();
    }
	public function skirts() {
        $this->set('emps', 'my skirts');
		//$this->display();
    }*/
}